package com.epam;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    private static final StringBuilder sb = new StringBuilder();
    private static boolean isLastDirectory;
    private static boolean isLastFile;

    public static void main(String[] args) {
        File folder = new File(args[0]);
        if (folder.isDirectory()) writeFile(folder);
        else readFile(folder);
    }

    private static void writeFile(File folder) {
        printDirectoryTree(folder, 0);
        try {
            FileUtils.writeStringToFile(
                    new File(folder.getAbsolutePath().concat("\\fileTree.txt")),
                    sb.toString(),
                    Charset.defaultCharset()
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Файл записан " + folder.getAbsolutePath().concat("\\fileTree.txt"));
    }

    private static void printDirectoryTree(File folder, int i) {
        writeLine(folder, i);
        File[] files = folder.listFiles();
        if (files != null) {
            Arrays.sort(files, Comparator.comparing(File::isDirectory));
            for (File file : files) {
                if (file.isDirectory()) {
                    isLastDirectory = file.equals(files[files.length - 1]);
                    printDirectoryTree(file, i + 1);
                } else {
                    isLastFile = file.equals(files[files.length - 1]);
                    writeLine(file, i + 1);
                }
            }
        }
    }

    private static void writeLine(File file, int i) {
        if (file.isDirectory()) {
            appendIndent(i);
            appendDirectoryPrefix(i);
        } else if (file.isFile()) {
            appendIndent(i - 1);
            appendFilePrefix();
            appendIndent(Math.min(2, i));
        }
        appendFileName(file, i);
    }

    private static void appendIndent(int i) {
        sb.append("    ".repeat(Math.max(0, i - 1)));
    }

    private static void appendDirectoryPrefix(int i) {
        if (!isLastDirectory && i > 0) {
            sb.append("├───");
        } else if (isLastDirectory) {
            sb.append("└───");
        }
    }

    private static void appendFilePrefix() {
        if (!isLastDirectory) {
            sb.append("│   ");
        } else {
            sb.append("    ");
        }
    }

    private static void appendFileName(File file, int i) {
        sb.append(file.getName());
        if (isLastFile && !isLastDirectory) {
            sb.append("\n");
            appendIndent(i - 1);
            sb.append("│   ");
            isLastFile = false;
        }
        sb.append("\n");
    }

    private static void readFile(File file) {
        Scanner sc;
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        String line;
        int folders = 0;
        int files = -1;
        int nameLength = 0;
        while (sc.hasNextLine()) {
            line = sc.nextLine().trim();
            if (line.startsWith("├") || line.startsWith("└")) {
                folders++;
            } else if (line.startsWith("│")) {
                int length = line.replaceAll("│", "").trim().length();
                if (length > 0){
                    nameLength += length;
                    files++;
                }
            } else {
                int length = line.trim().length();
                if (length > 0) {
                    nameLength += length;
                    files++;
                }
            }
        }
        System.out.println("Всего папок: " + folders);
        System.out.println("Всего файлов: " + files);
        System.out.println("Среднее количество файлов в папке: " + (double) files / folders);
        System.out.println("Средняя длинна названия файла: " + (double) nameLength/files);
    }
}